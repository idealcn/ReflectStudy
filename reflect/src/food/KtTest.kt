package food

class KtTest {

    fun test1() {

        val superType: GenericType<in Apple> = GenericType()
        superType.data = Fruit()
        superType.data = Food()
        // 这里可以加入HongFuShi，是因为，HongFuShi可以向上自动转型为Apple
        superType.data = HongFuShi()

        val data : Any = superType.data


        val extendsType : GenericType<out Fruit> = GenericType()
        extendsType.data = Fruit()
        extendsType.data = Apple()
        // 限定了上界为Fruit，因此不能使用Food
       // extendsType.data = Food()
        //限定了上界为Fruit，因此可以用Fruit来接收
        val fruit : Fruit = extendsType.data
    }

}