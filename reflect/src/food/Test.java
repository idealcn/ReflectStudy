package food;

public class Test {
    public static void main(String[] args) {
        testFood();
    }



    private static <T extends Apple> void setExtendFood(T food){

    }
    private static void setExtendFood(GenericType<? extends Fruit> food){
        Fruit data = food.getData();
    }

//    private static <T extends Apple> void setExtendFood(food.GenericType<T> food){
//
//    }


    private static void setFruit(GenericType<Fruit> fruit) {

    }

    private static  void setSuperFood(GenericType<? super Apple> type){
        Object data = type.getData();
    }


    private static void testFood() {
        GenericType<? super Apple> genericType = new GenericType<>();
        // 可以写,，因为HongFuShi可以向上转型成Apple或者其父类。
        genericType.setData(new HongFuShi());
//        genericType.setData(new Fruit());
        // 读出来不能得到具体类型
        Object data = genericType.getData();


        final GenericType<? extends Apple> extendGenericType = new GenericType<>();
//        extendGenericType.setData(new Apple());
//        extendGenericType.setData(new HongFuShi());
        // 只能读不能写
        Apple apple = extendGenericType.getData();
//        extendGenericType.setData(new Fruit());


//        setFruit(new food.GenericType<Apple>());
//        setExtendFood(new Fruit());
        setExtendFood(new HongFuShi());
        setExtendFood(new GenericType<Apple>());
//        setExtendFood(new food.GenericType<Food>());
        setSuperFood(genericType);
        setSuperFood(new GenericType<Fruit>());
//        setSuperFood(new food.GenericType<HongFuShi>());
    }
}
