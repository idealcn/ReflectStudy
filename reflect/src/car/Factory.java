package car;

public interface Factory {
    Car make(String type);
}
