package car;

import java.lang.reflect.Method;

public class PrintTool {

    public static void printMethods(Method... methods) {
        System.out.println("--------method start-----------------");
        for (Method method : methods) {
            System.out.println("methodName: "+method.getName()+", returnType: "+method.getReturnType().getName());
        }
        System.out.println("--------method end-----------------");
    }

}
