package car;

import java.lang.reflect.*;
import java.util.List;

public class ReflectMain {
    public static void main(String[] args)  {


        /*try {
            testBoyFriend();
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
           // throw new RuntimeException(e);
            e.printStackTrace();
        }*/


       // testStudent();

       // testReturnType();




    }


    private static void testReturnType() {
        Class<Student> clazz = Student.class;
        try {
            Method method = clazz.getMethod("getTList");
            Class<?> returnType = method.getReturnType();
            System.out.println("returnType: "+returnType.getTypeName());
            System.out.println(" returnType.isArray(): "+ returnType.isArray());
                System.out.println("typeParameter=> ");
                for (TypeVariable<? extends Class<?>> typeParameter : returnType.getTypeParameters()) {
                    System.out.println("typeName: "+  typeParameter.getTypeName());
                    System.out.println(typeParameter.getGenericDeclaration().getTypeName());
                    System.out.println("bound-------------------");
                    for (Type bound : typeParameter.getBounds()) {
                        System.out.println("typeName: "+bound.getTypeName());
                    }
                    System.out.println("--------------------bound");
                }
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    private static void testBoyFriend() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        BoyFriend boyFriend = new BoyFriend();
        Class<?> boyFriendClass = boyFriend.getClass();//car.BoyFriend.class;
        String typeName = boyFriendClass.getTypeName();
        System.out.println("typeName=>"+typeName);
        TypeVariable<? extends Class<?>>[] boyFriendClassTypeParameters = boyFriendClass.getTypeParameters();
        System.out.println("-----------boyFriendClassTypeParameter:");
        for (TypeVariable<? extends Class<?>> boyFriendClassTypeParameter : boyFriendClassTypeParameters) {
            System.out.println("typeName: "+boyFriendClassTypeParameter.getTypeName());
            for (Type bound : boyFriendClassTypeParameter.getBounds()) {
                System.out.println("bound=>"+bound.getTypeName());
            }
            System.out.println("genericDeclaration: "+boyFriendClassTypeParameter.getGenericDeclaration().getTypeName());
        }
        System.out.println("boyFriendClassTypeParameter:------------");

        /**
         * 所有public的方法，包含从父类继承来的
         */
        Method[] methods = boyFriendClass.getMethods();
        PrintTool.printMethods(methods);
        /**
         * 自身所有的方法，不限于public；如果重写了父类的方法，也返回。
         */
        Method[] declaredMethods = boyFriendClass.getDeclaredMethods();
        PrintTool.printMethods(declaredMethods);

        Method enclosingMethod = boyFriendClass.getEnclosingMethod();

        Method eat = boyFriendClass.getMethod("eat");

        Method enclosingMethod1 = new Factory() {
            @Override
            public Car make(String type) {
                return null;
            }
        }.getClass().getEnclosingMethod();

        eat.invoke(boyFriend);
    }

    private static void testStudent() {
        //-------------------------------------------------
        Class<Student> clazz = Student.class;
        //
        try {
            Method method = clazz.getMethod("getMultiType", List.class,Weather.class,Friend.class);
            Class<?> returnType = method.getReturnType();
            System.out.println("method return type=>"+returnType.getTypeName());
            TypeVariable<? extends Class<?>>[] methodReturnTypeParameters = returnType.getTypeParameters();
            for (TypeVariable<? extends Class<?>> methodReturnTypeParameter : methodReturnTypeParameters) {
                System.out.println("methodReturnTypeParameter: typeName=>" +methodReturnTypeParameter.getTypeName()+",genericDeclaration=>"+methodReturnTypeParameter.getGenericDeclaration()
                        +",name=>"+methodReturnTypeParameter.getName());
                for (Type bound : methodReturnTypeParameter.getBounds()) {
                    System.out.println("bound=>"+bound.getTypeName()+","+bound.getClass().getTypeName());
                }
            }

            Type genericReturnType = method.getGenericReturnType();
            if (genericReturnType instanceof ParameterizedType){
                ParameterizedType parameterizedType = (ParameterizedType) genericReturnType;
                Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
                Type ownerType = parameterizedType.getOwnerType();
                Type rawType = parameterizedType.getRawType();
                for (Type actualTypeArgument : actualTypeArguments) {
                    if (actualTypeArgument instanceof ParameterizedType){
                        ParameterizedType parameterizedType1 = (ParameterizedType) actualTypeArgument;
                        Type[] actualTypeArguments1 = parameterizedType1.getActualTypeArguments();
                        int length = actualTypeArguments1.length;
                    }
                }
            }
            Type[] genericParameterTypes = method.getGenericParameterTypes();
            ParameterizedType parameterizedType = (ParameterizedType) genericParameterTypes[0];
            Class<?>[] parameterTypes = method.getParameterTypes();
            TypeVariable<Method>[] typeParameters = method.getTypeParameters();
            for (TypeVariable<Method> typeParameter : typeParameters) {

            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        //-------------------------------------------------
    }
}
