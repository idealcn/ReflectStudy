package car;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Student  implements People{
    private String name;
    private int age;
    private List<Friend> friendList = new ArrayList<>();
    private float[] scoreArr = new float[5];
    public Student(){}
    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() throws NoStudentNameFoundException {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int add(int a,int b){
        return a+b;
    }

    public List<Friend> getFriendList(){
        return friendList;
    }

    public void setFriendList(List<Friend> friendList) {
        this.friendList = friendList;
    }


    public void setScoreArr(float[] scoreArr) {
        this.scoreArr = scoreArr;
    }

    public <T extends Friend> T[] getScoreArr() {
        return null;
    }

    public <T extends Friend> List<T> getTList(){
        return null;
    }


    public <T extends Friend,D> List<Map<D,T>>  getMultiType(List<Map<D,T>> list,Weather weather,T t){
        return list;
    }

    public <T> void setWindow(Window.Builder<T> builder){

    }

    @Override
    public void speak() {

    }

    @Override
    public void eat() {

    }
}
