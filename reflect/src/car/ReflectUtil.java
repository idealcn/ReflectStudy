package car;

import java.lang.reflect.*;

public class ReflectUtil {

    /**
     * 获取方法返回值泛型的类型
     * @param clazz
     * @param methodName
     */
    public static void getMethodGenericReturnType(Class<?> clazz, String methodName) {
        try {
            Method method = clazz.getMethod(methodName);
            //获取方法返回值的类型
            Type type = method.getGenericReturnType();
            getActualTypeArguments(type);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    /**
     * Type有一个子类，四个子接口。要逐一判断。
     * @param type
     */
    private static void getActualTypeArguments(Type type) {
        //判断通配符?
        if (type instanceof WildcardType){
            WildcardType wildcardType = (WildcardType) type;
            Type[] upperBounds = wildcardType.getUpperBounds();
            Type[] lowerBounds = wildcardType.getLowerBounds();
            if (null!=upperBounds){
                for (Type upperBound : upperBounds) {
                    System.out.println("upperBound: "+upperBound.toString());
                   // System.out.println("upperBound： "+upperBound.getTypeName());
                    getActualTypeArguments(upperBound);
                }
            }
        }
        else if (type instanceof GenericArrayType){ //T[]
            //数组的类型必须带泛型
            GenericArrayType genericArrayType = (GenericArrayType) type;
            Type genericComponentType = genericArrayType.getGenericComponentType();
            getActualTypeArguments(genericComponentType);
        }
        else if (type instanceof TypeVariable){ // T
                TypeVariable typeVariable = (TypeVariable) type;
            Type[] bounds = typeVariable.getBounds();
            for (Type bound : bounds) {
                getActualTypeArguments(bound);
            }
            GenericDeclaration genericDeclaration = typeVariable.getGenericDeclaration();
            String name = typeVariable.getName();
        }
        else if (type instanceof ParameterizedType) { //List<String>
            ParameterizedType parameterizedType = (ParameterizedType) type;
            getActualTypeArguments(parameterizedType);
        }
        else if (type instanceof Class) {
            try {
                Class<?> clazz = (Class<?>) type;
                // TODO: 2019/3/21 数组在这里初始化失败
                /*
                不能出现在数组的初始化中，即new数组之后不能出现，否则javac无法通过
                 */
                boolean isArray = clazz.isArray();
                boolean isInterface = clazz.isInterface();
                boolean isPrimitive = clazz.isPrimitive();
                boolean isAnnotation = clazz.isAnnotation();
                boolean isEnum = clazz.isEnum();
                if (isAnnotation || isInterface || isPrimitive || isArray || isEnum){
                    return;
                }
                Object o = clazz.newInstance();
                System.out.println(o.toString());
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }

        }
    }


    private static void getActualTypeArguments(ParameterizedType parameterizedType) {
        Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
        if (actualTypeArguments.length > 0) {
            for (Type actualTypeArgument : actualTypeArguments) {
                getActualTypeArguments(actualTypeArgument);
            }
            Type rawType = parameterizedType.getRawType();
            if (null!=rawType) {
                System.out.println("rawType: " + rawType);
            }
            Type ownerType = parameterizedType.getOwnerType();
            if (null!=ownerType) {
                System.out.println("ownerType: " + ownerType);
            }
        }
    }


    public static void getMethodParameterType(Class<?> clazz, String methodName)  {
        try {
            Method method = clazz.getMethod(methodName, Window.Builder.class);
            //
            Type[] genericParameterTypes = method.getGenericParameterTypes();
            for (Type genericParameterType : genericParameterTypes) {
                String typeName = genericParameterType.getTypeName();
                if (genericParameterType instanceof TypeVariable){
                    TypeVariable typeVariable = (TypeVariable) genericParameterType;
                }else  if (genericParameterType instanceof ParameterizedType){
                    ParameterizedType parameterizedType = (ParameterizedType) genericParameterType;
                    Type ownerType = parameterizedType.getOwnerType();
                    Type rawType = parameterizedType.getRawType();
                    Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
                    for (Type actualTypeArgument : actualTypeArguments) {
                        if (actualTypeArgument instanceof TypeVariable){
                            TypeVariable typeVariable = (TypeVariable) actualTypeArgument;
                            String name = typeVariable.getName();
                            Type[] bounds = typeVariable.getBounds();
                            GenericDeclaration genericDeclaration = typeVariable.getGenericDeclaration();
                            TypeVariable<?>[] typeParameters = genericDeclaration.getTypeParameters();
                            for (TypeVariable<?> typeParameter : typeParameters) {
                                String typeParameterName = typeParameter.getName();
                            }


                        }
                    }
                }
            }

            /**
             * 方法参数类型，不包含参数携带的泛型
             */
            Class<?>[] parameterTypes = method.getParameterTypes();
            for (Class<?> parameterType : parameterTypes) {
                String name = parameterType.getName();
            }
            /**
             * 方法参数
             */
            Parameter[] parameters = method.getParameters();
            for (Parameter parameter : parameters) {
                String name = parameter.getName();
            }
            /**
             * 方法参数的泛型类型
             * 返回TypeVariable<Method>表明 泛型是在方法上声明的。
             */
            TypeVariable<Method>[] typeParameters = method.getTypeParameters();
            for (TypeVariable<Method> typeParameter : typeParameters) {
                String name = typeParameter.getName();
            }
            Type genericReturnType = method.getGenericReturnType();
            String typeName = genericReturnType.getTypeName();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

    }
}
